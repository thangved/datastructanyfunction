# Một số hàm sử dụng tính nhanh trắc nghiệm CTDL

> Một số hàm sử dụng tính nhanh trắc nghiệm CTDL biên soạn bởi một con VỊT
>
> Tất cả nằm trong `./src`

## calculate

> Đây là các chương trình tính giá trị của các chuỗi tiền tố, hậu tố.

- pos: tính giá trị của chuỗi hậu tố.
- pre: tính giá trị của chuỗi tiền tố.

## convert

> Chuyển đổi qua lại giữa tiền, trung, hậu tố.

- intopos: chuyển trung tố sang hậu tố.
- pretopos: chuyển tiền tố sang hậu tố.

## converttree

> Dựng cây từ chuỗi trung tố và hậu tố.

## levelorder

> Duyệt cây theo mức
