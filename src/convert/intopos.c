#include <stdio.h>
#include <string.h>
#include <stdio.h>

#define MaxLength 100
typedef int ElementType;

typedef struct
{
    ElementType Elements[MaxLength];
    int Top_idx;
} Stack;

void makenullStack(Stack *pS)
{
    pS->Top_idx = MaxLength;
}

int emptyStack(Stack S)
{
    return S.Top_idx == MaxLength;
}

int fullStack(Stack S)
{
    return S.Top_idx == 0;
}

ElementType top(Stack S)
{
    return S.Elements[S.Top_idx];
}

void pop(Stack *pS)
{
    pS->Top_idx++;
}

void push(ElementType x, Stack *pS)
{
    pS->Top_idx--;
    pS->Elements[pS->Top_idx] = x;
}

int isNumber(char c)
{
    return (c >= '0' && c <= '9');
}

int isAb(char c)
{
    return (c >= 'a' && c <= 'z');
}

void pushChar(char c, char *s)
{

    s[strlen(s) + 1] = '\0';
    s[strlen(s)] = c;
}

int isOP(char c)
{
    return (c == '+' || c == '-' || c == '*' || c == '/');
}

int mucUutien(char c)
{
    return c == '*' || c == '/' ? 1 : 0;
}

void chuyenHauto(char *trungto, char *hauto)
{
    hauto[0] = '\0';
    Stack S;
    makenullStack(&S);
    for (int i = 0; i < strlen(trungto); i++)
    {
        if (trungto[i] == ' ')
        {
        }
        else if (isNumber(trungto[i]) || isAb(trungto[i]))
            pushChar(trungto[i], hauto);
        else if (trungto[i] == '(')
            push('(', &S);
        else if (trungto[i] == ')')
        {
            while (top(S) != '(')
            {
                pushChar(top(S), hauto);
                pop(&S);
            }
            pop(&S);
        }
        else if (isOP(trungto[i]))
        {
            while (!emptyStack(S) && top(S) != '(' && mucUutien(trungto[i]) <= mucUutien(top(S)))
            {
                pushChar(top(S), hauto);
                pop(&S);
            }
            push(trungto[i], &S);
        }
    }
    while (!emptyStack(S))
    {
        pushChar(top(S), hauto);
        pop(&S);
    }
}

int main()
{
    char pos[100];
    pos[0] = '\0';
    chuyenHauto((char *)"(a * (5 + b) – 2 * c) + 4", pos);
    puts(pos);
    return 0;
}