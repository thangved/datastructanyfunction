#include "avlqueue.h"

using namespace std;

void levelOrder(tree root)
{
    Queue *queue = createQueue(100);

    enqueue(queue, root);
    while (!isEmpty(queue))
    {
        tree node = dequeue(queue);
        if (node->left != NULL)
            enqueue(queue, root->left);
        if (node->right != NULL)
            enqueue(queue, node->right);
        cout << node->key << endl;
    }
}

int main()
{
    tree root = newNode(20);
    root = insert(root, 100);
    root = insert(root, 10);
    root = insert(root, 99);
    root = insert(root, 90);
    root = insert(root, 77);
    levelOrder(root);
    return 0;
}