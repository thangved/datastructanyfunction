#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef int DataType;
struct Node
{
    DataType Data;
    struct Node *Left, *Right;
};
typedef struct Node *Tree;

Tree initTree()
{
    return NULL;
}

int isEmpty(Tree T)
{
    return T == NULL;
}

int getHeight(Tree T)
{
    if (T == NULL)
        return -1;
    int lh = getHeight(T->Left);
    int rh = getHeight(T->Right);

    return 1 + (lh > rh ? lh : rh);
}

int findIndex(DataType x, char in[], int star, int end)
{
    int i = star;
    while (i <= end)
    {
        if (x == in[i])
            return i;
        else
            i++;
    }
    return i;
}

void inOrder(Tree T)
{
    if (T == NULL)
        return;
    inOrder(T->Left);
    printf("%d ", T->Data);
    inOrder(T->Right);
}

int CURRENT_INDEX = 0;
Tree createTree(char *pre, char *in, int start, int end)
{
    if (start > end)
        return NULL;
    Tree T = (Tree)malloc(sizeof(struct Node));
    if (start == end)
    {
        T->Data = pre[CURRENT_INDEX];
        T->Left = NULL;
        T->Right = NULL;
        CURRENT_INDEX++;
        return T;
    }
    T->Data = pre[CURRENT_INDEX];
    int i = findIndex(pre[CURRENT_INDEX], in, start, end);
    CURRENT_INDEX++;
    T->Left = createTree(pre, in, start, i - 1);
    T->Right = createTree(pre, in, i + 1, end);
    return T;
}

int length(char *s)
{
    int i = 0;
    while (s[i])
        i++;
    return i;
}

void reverse(char string[])
{
    int len = length(string);
    for (int i = 0; i < len / 2; i++)
    {
        char temp = string[i];
        string[i] = string[len - i - 1];
        string[len - i - 1] = temp;
    }
}

int main()
{
    char in[] = "25461798103";
    char pos[] = "56429108731";
    reverse(pos);
    Tree t = createTree(pos, in, 0, 11);
    inOrder(t);

    return 0;
}